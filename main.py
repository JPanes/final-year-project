import cv2
import imageio
import glob
import os
import datetime
import copy
import random
from PIL import Image

import read_board
import Board as halma
import agent

"""
Created by Joe Benjamin Hopkins Panes, 961006, 2020

*********************
PYTHON VERSION: 3.7.4

Built using the Anaconda distribution 3.7, October 2019

MODULE VERSIONS: 
 ______________________________________
|  Module                 |  Version   |
|--------------------------------------|
|  opencv-contrib-python  |  4.2.0.32  |            
|  Pillow                 |  6.2.0     |
|  imageio                |  2.5.0     |
|  numpy                  |  1.16.2    |
| ________________________|____________|       
*********************

"""


PLAYER_ONE_PIECE = "w"
PLAYER_TWO_PIECE = "b"
MAX_DEPTH = 3

#Musical responses
RESPONSES = ["https://www.youtube.com/watch?v=FnkTuHP9q3o", "https://www.youtube.com/watch?v=Jw9gHAmObps", "https://www.youtube.com/watch?v=eYYUAib5EWo"
             "https://www.youtube.com/watch?v=qaJYclpNwUE", "https://www.youtube.com/watch?v=Pq4SSlsZ_p0", "https://www.youtube.com/watch?v=j5NwJpo9Gng"
             "https://www.youtube.com/watch?v=ud6LiVJkwyA", "https://www.youtube.com/watch?v=dQw4w9WgXcQ", "https://www.youtube.com/watch?v=jjFHqMxLGjg"]

def convertPositionStateToText(position):
    """
    Takes in the current position state then converts it from a single character to words

    INPUT:
        :param position: A string of either "w", "b", "-"

    Output:
        String, of either "a white piece", "a black piece" or "an empty space" respectively, or an error if none of those are entered
    """
    if position == "w":
        return "a white piece"
    elif position == "b":
        return "a black piece"
    elif position == "-":
        return "an empty space"
    else:
        return False

def performTurn(virtBoard, realBoard, webcam, corners, playerPiece, oppPiece, isAgent, needHelp, agentType):
    """
    Perform the player's move virtually and on the real board, then compare

    INPUT:
        :param virtBoard: List of lists, containing both player pieces and empty spaces
        :param realBoard: List of lists, containing both player pieces and empty spaces
        :param webcam: Video object, allows the interface between the program and the webcam
        :param corners: Numpy array, contains the c-ords for all inner squares with in the image
        :param playerPiece: String, the colour of the current player's pieces
        :param oppPiece: String, the colour of the opponent player's pieces
        :param isAgent: Boolean, whether the current player is a human opponent or the computer
        :param needHelp: Boolean, whether the player wants to specifiy co-ordinates to see potential moves and move pieces based on that
        :param agentType: Integer, what type of agent will be used (if any), where 1 is Minmax and 2 is random

    Output:
        List of lists of the new board state, and a boolean as to whether it has been successful
    """
    if isAgent == False:
        if needHelp == True:
            print("ATTENTION - If you wish to switch from this mode, press Ctrl+C now.")
            input("The effects of pressing it later are not guarranteed to provide the desired effect and may instead, end the game")
            hasJumped = False
            hadTurn = False

            destX = None
            destY = None
            #Play until piece hasn't jumped
            while hasJumped == True or hadTurn == False:
                playerAction = halma.performPlayerTurn(playerPiece, virtBoard, hasJumped, destX, destY)

                currX,currY = playerAction[0]
                destX,destY,hasJumped = playerAction[1]
                
                virtBoard = halma.movePiece(currX, currY, destX, destY, virtBoard)
                
                #Allows atleast one move to happen
                hadTurn = True
        else:
            #if they are interacting mainly with the board, then there are a lot less checks involved
            performedMove = False
            while performedMove == False:
                input("When you have moved the piece on the board, press enter")
                realBoard, virtBoard, performedMove = getNewBoardState(webcam, corners, playerPiece, virtBoard)
            
            return realBoard, performedMove

    elif isAgent == True:
        #Depending upon the agent selected, play differently
        if agentType == 1:
            bestValue = agent.performMinMax(virtBoard, MAX_DEPTH, playerPiece, oppPiece)
            virtBoard = bestValue[1]
        
        elif agentType == 2:
            virtBoard = agent.makeARandomMove(virtBoard, playerPiece)

        #Only print the board before asking the user to make a move with the agent, since if they are getting help, it will be printed anyway
        print("The new board state looks as follows")
        halma.printBoard(virtBoard)

    if isAgent == True or needHelp == True:        

        movedPieceCorrectly = False

        #Compare virtBoard to realBoard to see if both are the same (thus accurate)
        while (movedPieceCorrectly == False):
            input("When you have moved the piece on the board, press enter")
            #Get the new state of the realBoard
            worked = takeImageOfBoard(webcam, corners)
            
            if worked:
                #Get new board state
                realBoard = read_board.convertBoardToList("square")
                #Check whether the realBoard looks as expected
                movedPieceCorrectly = findDifferences(virtBoard, realBoard)
    
    return realBoard, movedPieceCorrectly

def reinitialise(webcam, virtBoard):
    """
    In the case of the board getting budged during play, allows a quick re-establishment of the board structure
    
    INPUTS:
        :param webcam: Video object, allows interaction between a webcam plugged into the computer
        :param virtBoard: 2D list of lists, the current board state prior to the camera being nudged
    
    OUTPUT:
        A boolean, potentially a list of lists containing the points to each square's corner (provided that the board it find is what it was expecting)
    """
    try:
        worked = False
        while worked == False:
            found, corners = read_board.initialise(webcam)

            if found == False:
                print("No board found, plz fix human")
            else:
                print("<--The-Board-State-->")
                halma.printBoard(virtBoard)
                print("<------------------->")

        
                input("Please press enter when you have filled the board, and are ready to proceed")
                worked = takeImageOfBoard(webcam, corners)


        realBoard = read_board.convertBoardToList("square")

        if realBoard == virtBoard:
            return True, corners
    except:
        print("---Attention---")
        print("The setup of either the environment or camera has resulted in a failure during initialisation, please adjust through the following:")
        print("     1. Ensure the board is in a reasonable level of light")
        print("     2. The webcam is pointed towards the centre of the board")
        print("     3. The image of the board ")
    


    return False, []

def takeImageOfBoard(webcam, corners):
    """
    Using the webcam, take an image of the board and break it down into sub-images containing each square

    INPUTS:
        :param webcam: Video object, allows the interface between the webcam and program
        :param corners: Numpy array, contains the c-ords for all inner squares with in the image

    OUTPUT:
        returns a boolean as to whether it was successful in taking an image and breaking it down
    """
    #Capture image
    check2, frame2 = webcam.read()

    imageio.imwrite("currBoardPure.jpg", frame2, quality=100)
    pureImage = Image.open("currBoardPure.jpg")

    worked = read_board.getEachSquareFromImage(pureImage, corners, "square")

    if worked:
        return worked
    else:
        print("There was an error when getting the squares from the image")
        return False

def getNewBoardState(webcam, corners,  currPlayer, virtBoard):
    """
    Get the new position from the current board state, also check whether it is feasible to get to that state

    INPUTS:
        :param webcam: Video object, allows the interface between the webcam and program
        :param corners: Numpy array, contains the c-ords for all inner squares with in the image
        :param currPlayer: String, the current players piece colour
        :param virtBoard: 2D list, the previous state of the board

    OUTPUT:
        returns either a False boolean or the new board states if the move was valid
    """
    worked = takeImageOfBoard(webcam, corners)

    if worked == True:
        #Create two boards for comparison of whether moves are being performed properly
        realBoard = read_board.convertBoardToList("square")
        
        #Based upon the virtual board, check whether the move is valid
        moves = agent.getAllPotentialMoves(currPlayer, virtBoard)

        potentialMoves = {}
        
        for currPiece in range(0, len(moves)):
            for currMove in range(1, len(moves[currPiece][2])):
                currBoard = copy.deepcopy(realBoard) 
                #Extract move information from the list
                currCoX, currCoY = moves[currPiece][1]
                newCoX, newCoY, _ = moves[currPiece][2][currMove]
                
                newBoard = halma.movePiece(currCoX, currCoY, newCoX, newCoY, currBoard)
                
                if str(newBoard) not in potentialMoves:
                    potentialMoves[str(newBoard)] = "Yep"
        #Check whether the move made by the user, is valid
        if potentialMoves.get(str(realBoard)):
            virtBoard = realBoard.copy()
            return realBoard, virtBoard, True

        else:
            print("You have provided an invalid move, is this correct?")
            inp = input("Is the program wrong? (yes/no)")

            if inp.lower() == "y" or inp.lower() == "yes":
                virtBoard = realBoard.copy()
                return realBoard, virtBoard, True
            else:
                return virtBoard, virtBoard, False

    else:
        print("There was an issue when reading the image of the board, please check")

def findDifferences(virtBoard, realBoard):
    """
    Compare the two board states and confirm whether the real world board is the same as the virtual board
    But, most importantly, if not, specifcally point to the positions that need adjustment

    INPUTS:
        :param virtBoard: 2D list, the current board state of the virtual board used for decision making by the computer and assistively for the human player
        :param realBoard: 2D list, the current board state of the real world board as of the last image taken
    OUTPUT:
        returns a boolean on whether there exists any difference between the two boards
    """
    differences = []
    for y in range(0, halma.BOARD_MAX_SIZE):
        for x in range(0, halma.BOARD_MAX_SIZE):
            #If difference get relevant inforamtion
            if virtBoard[y][x] != realBoard[y][x]:
                atPositionVirt = virtBoard[y][x]
                atPositionReal = realBoard[y][x]
                differences.append((x,y, atPositionVirt, atPositionReal))

    #Check accuracy
    if len(differences) == 0:
        return True
    else:    
        halma.printBoard(virtBoard)
        print("There are differences between the board namely at positions: ")
        #Go through list and show differences between the two board states
        for curr in differences:
            virtPosition = convertPositionStateToText(curr[2])
            realPosition = convertPositionStateToText(curr[3])

            print(f"At position {curr[0]}, {curr[1]} {virtPosition} was expected, but {realPosition} was discovered instead")

        input("Please update the real board to reflect the above")
        return False

def showRules():
    """
    Intended as a quick and easy way of showing the rules to the person, prints everthing to the command line

    INPUTS:
        N/A
    OUTPUT:
        N/A
    """
    print("")
    print(">------Rules-----<")
    print("Halma (Greek for jump) is a 19th century German game, where the goal is to get your pieces to the opposing side of the board")
    print("before your opponent.")

    print("A more well known variant of Halma is Chinese Chequers (originally called Star Halma), which is a board variant.")

    print("----Movement----")
    print("There are two method of moving within the game of Halma:")
    print("1. Into adjacent spaces, provided there is an empty space next to a given piece, then that piece is able to move there (as indicated by *")
    print("   in the diagram below). However, if there is another piece there then you cannot move there.")
    print("| - | * | * | * | - |")
    print("| - | * |[w]| w | - |")
    print("| - | b | * | * | - |")
    print("")
    print("2. Jump over pieces, if there is a piece in an adjacent spot and there is an empty space on the other side of that piece opposite from the current piece,")
    print("   then the current piece is able to jump over that piece into the empty space (as indicated by ! in the diagram below).")
    print("| - | * | * | * | - |")
    print("| - | * |[w]| w | ! |")
    print("| - | b | * | * | - |")
    print("")
    print("2.1 Jumping over pieces allows continued movement, after jumping, provided you are able to jump over another piece, you can continue to do so")
    print("| - | b | b | - | - | - |")
    print("| - | ! | - | ! | - | ! |")
    print("| b | w | b | b | w | - |")
    print("| - | ! | b | ! | - | w |")
    print("| - | - | - | - | w | * |")
    print("| - | - | - | w | * |[w]|")

    print("")
    print("---How-To-Win---")
    print("As mentioned prior, the goal of the game is is to get your pieces from your side of the board into the opposing side.")
    print("Therefore, your goal is to get your pieces into the positions below, before your opponent can get theirs into the positions marked as !")
    print("(your starting position).")
    print("")
    print("| w | w | w | - | - | - |")
    print("| w | w | - | - | - | - |")
    print("| w | - | - | - | - | - |")
    print("| - | - | - | - | - | ! |")
    print("| - | - | - | - | ! | ! |")
    print("| - | - | - | ! | ! | ! |")

    print("And that is it, you now know the rules of Halma, if anything is unclear or if you have any questions feel free to ask anything.")

def getMenu(playerOneHelp, webcam, virtBoard, corners):
    """
    A simple method of getting extra information, 

    INPUTS:
        :param playerOneHelp: Boolean, whether the player is using the performPlayerTurn function during their turns
        :param webcam: Video object, an interface between the program and the webcam
        :param virtBoard: 2D list, the current board state
        :param corners: Numpy array, the coordinates for each of the 7x7 inner squares in the image
    OUTPUTS:
        returns the current coordinates for the corners of the squares within the board
    """
    print("Greetings, what would you like to do?")
    print("(A)bort game")
    print("Toggle (H)elp")
    print("(N)evermind, go back")
    print("Re(I)nitialise")
    print("(R)ules")

    inp = input("Which of the above do you wish to proceed with? ")

    if inp.lower() == "a":
        print("Thank you for playing")
        exit()
    elif inp.lower() == "h":
        if playerOneHelp:
            playerOneHelp = False
        else:
            playerOneHelp = True
    elif inp.lower() == "n":
        print("No problem")
    elif inp.lower() == "i":
        worked = False
        
        while worked == False:
            worked, corners = reinitialise(webcam, virtBoard)
    elif inp.lower() == "r":
        showRules()
    else:
        index = random.randint(0, len(RESPONSES)-1)
        print("I don't quite know how to respond to that, so have a song instead " + RESPONSES[index])
    
    return playerOneHelp, virtBoard, corners

#Initialise video object to use webcam
webcam = cv2.VideoCapture(0)

print("Greetings!")
print("If you aren't familar with the rules of Halma please see the handout given or press Ctrl+C")

replay = True
while replay != False:
    print("Hello, and welcome to Halma!")
    #Information related to how the game will be peformed    
    playerOneAgent = True
    playerTwoAgent = True

    playerOneHelp = False
    
    playerOneAgentType = None
    playerTwoAgentType = 1

    confirmedGameSetup = False
    while confirmedGameSetup == False:
        #Check whether the person is planning on playing the game
        inp = input("Are you planning on playing the game? (yes/no) ")
        if inp.lower() == "y" or inp.lower() == "yes":
            playerOneAgent = False

            inp = input("Do you want some help when playing? (yes/no) ")

            if inp.lower() == "yes" or inp.lower() == "y":
                playerOneHelp = True
                confirmedGameSetup = True
            elif inp.lower() == "no" or inp.lower() == "n":
                confirmedGameSetup = True

        else:
            inp = input("You have selected to not play, please confirm? (yes/no) ")

            if inp.lower() == "yes" or inp.lower() == "y":
                while confirmedGameSetup == False:
                    print("The currently implemented agents to take your stead are: ")
                    print("1. Minmax")
                    print("2. Random")
                    try:
                        inp = int(input("Please select a number: "))
                    except:
                        print("Please only enter a number")
                    if inp == 1:
                        playerOneAgentType = 1
                        confirmedGameSetup = True
                    elif inp == 2:
                        playerOneAgentType = 2
                        confirmedGameSetup = True
                    else:
                        print("Sorry, but that is not a valid number")

    """
    Initialisation
    """
    worked = False
    while worked == False:
        corners = None
        #Understand where each square is in the image
        if corners == None:
            _,corners = read_board.initialise(webcam)

        input("Please press enter when you have filled the board, and are ready to proceed")
        try:
            worked = takeImageOfBoard(webcam, corners)
        except:
            print("---Attention---")
            print("The setup of either the environment or camera has resulted in a failure during initialisation, please adjust through the following:")
            print("     1. Ensure the board is in a reasonable level of light")
            print("     2. The webcam is pointed towards the centre of the board")
            print("     3. The image of the board ")

    #Create two boards for comparison of whether moves are being performed properly
    realBoard = read_board.convertBoardToList("square")
    virtBoard = realBoard.copy()
    
    hasWon = False
    #For when the piece has jumped
    hasJumped = False

    destX = None
    destY = None

    #Plays the game until one of the players has reached a winning state
    while hasWon == False:
        performedHumanTurn = False
        print("Current turn: Player One")
        while performedHumanTurn == False:
            try:
                realBoard, performedHumanTurn = performTurn(virtBoard, realBoard, webcam, corners, PLAYER_ONE_PIECE, PLAYER_TWO_PIECE, playerOneAgent, playerOneHelp, playerOneAgentType)
            except KeyboardInterrupt:
                playerOneHelp, virtBoard, corners = getMenu(playerOneHelp, webcam, virtBoard, corners)


        if(halma.winCheck(PLAYER_ONE_PIECE, realBoard)):
            print("Well done Player One!")
            hasWon = True
        else:
            virtBoard = realBoard.copy()
            peformedComputerTurn = False
            print("Current turn: Player Two")
            while peformedComputerTurn == False:
                try:
                    #The second player will always be an agent, playing using the Minmax algorithm
                    realBoard, peformedComputerTurn = performTurn(virtBoard, realBoard, webcam, corners, PLAYER_TWO_PIECE, PLAYER_ONE_PIECE, playerTwoAgent, False, playerTwoAgentType)
                except KeyboardInterrupt:
                    playerOneHelp, virtBoard, corners = getMenu(playerOneHelp, webcam, virtBoard, corners)

            if(halma.winCheck(PLAYER_TWO_PIECE, realBoard)):
                print("Well done Player Two!")
                hasWon = True
            else:
                virtBoard = realBoard.copy()
webcam.release()