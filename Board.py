"""
Created by Joe Benjamin Hopkins Panes, 961006, 2020

*********************
PYTHON VERSION: 3.7.4

BUILT USING ANACONDA VERSION: 3.7, October 2019

*********************
"""

BOARD_MIN_SIZE = 0
BOARD_MAX_SIZE = 6
MAX_PIECE_NO = 6

#The positions that contribute to a winning state
BLACK_WINNING_POSITIONS = {"5,3":(5,3), "4,4":(4,4), "5,4":(5,4), "3,5":(3,5), "4,5":(4,5), "5,5":(5,5)}
WHITE_WINNING_POSITIONS = {"0,0":(0,0), "1,0":(1,0), "2,0":(2,0), "0,1":(0,1), "1,1":(1,1), "0,2":(0,2)}

def initBoard ():
    """
    Returns an array that is the starting state of Chequers
    
    INPUT:
        None
    
    OUTPUTs:
        2D list of strings, containing either b, w or -, placed in the start state        
    """
    board = [["b","b","b","-","-","-"],
            ["b","b","-","-","-","-"],
            ["b","-","-","-","-","-"],
            ["-","-","-","-","-","w"],
            ["-","-","-","-","w","w"],
            ["-","-","-","w","w","w"]]

    return board

def findPiecePositions(board, colPiece):
    """
    Find the position of all pieces of a certain colour

    INPUTS:
        :param board: 2D list, the current board state
        :param colPiece: String, the colour of the pieces to be found
    OUTPUT:
        List, containing the co-ordinates of all pieces of a certain colour
    """
    piecePositions = []
    pieceCounter = 0

    for y in range(0, BOARD_MAX_SIZE):
        for x in range(0, BOARD_MAX_SIZE):
            if board[y][x] == colPiece:
                piecePositions.append((x,y))
                #When it has found all pieces
                if pieceCounter >= MAX_PIECE_NO:
                    y = BOARD_MAX_SIZE
                    break
                    
    return piecePositions

def printBoard(board):
    """
    Print out the board to look similar to the physical game, adding numbers to the rows and columns to be used like in chess.
    
    INPUT:
        :param board: 2D list, The current board state
    
    OUTPUT:
        Prints out the current board state
    """
    # Print out the board
    numLst = list(range(BOARD_MIN_SIZE, BOARD_MAX_SIZE))
    i = 0

    #Print out the y-coordinates of the column 
    numLine = "    "
    for num in numLst:
        numLst[i] = str(num)
        numLine += numLst[i]
        numLine += "   "
        
        i += 1
    print(numLine)

    #Print out the row, x-coordinates to the start
    i = 0
    for row in board:
        line = numLst[i]
        
        line +=  " | "
        for num in row:
            line += num + " | "
        i += 1
        print(line)

def movePiece(currCoX, currCoY, destCoX, destCoY, board):
    """
    Move the selected piece from the current co-ordinates to the destination co-ordinates on the board
    
    INPUT:
        :param currCoX: Integer, the x coordinate of where the piece is located on the board
        :param currCoY: Integer, the y coordinate of where the piece is located on the board
        :param destCoX: Integer, the x coordinate of the position to be moved to on the board
        :param destCoY: Integer, the y coordinate of the position to be moved to on the board
        :param board: 2D list, the current board state

    OUTPUT:
        2D list of strings, the new board state
        
    """
    #Get the type of piece that is being moved
    piece = board[currCoY][currCoX]
    
    #Make current spot empty
    board[currCoY][currCoX] = "-"
    
    #Place piece in new spot
    board[destCoY][destCoX] = piece

    return board

def getPotentialMoves(currCoX, currCoY, board, hasJumped):
    """
    Check whether the selected piece can perform a legal move.
    
    INPUT:
        :param currCoX: Integer, the x coordinate of the board position
        :param currCoY: Integer, The y coordinate of the board position
        :param board: 2D list, The current board state
        :param hasJumped: Boolean, whether or not the piece has moved across another piece before
    
    Output:
        A list of tuples, coordinates of legal move(s) that can be performed from the current position along whether or not a jump was performed prior
    """
    moves = []
    if board[currCoY][currCoX] == "-":
        return moves
    elif hasJumped == False:
        #Check area around piece
        for y in range(0,3):
            for x in range(0,3):
                newX = None
                newY = None

                newY = currCoY - 1 + y
                newX = currCoX - 1 + x

                if checkCoOrdValid(newX, newY) and coOrdsNotTheSame(currCoX, currCoY, newX, newY):
                    #Add to potential moves if valid
                    if isEmpty(newX, newY, board) == True:
                        moves.append((newX, newY, False))
                    #Check if empty space on the otherside of piece in selected spot
                    else:
                        #Work out new co-ordinates
                        newX = currCoX + calculateJumpPosition(x)
                        newY = currCoY + calculateJumpPosition(y)

                        if checkCoOrdValid(newX, newY) and isEmpty(newX, newY, board) == True:
                            moves.append((newX, newY, True))
    #If they have moved, only allow them to jump over other pieces to move
    else:
        #Check around piece if they have move before
         for y in range(0,3):
            for x in range(0,3):
                newX = None
                newY = None

                newY = currCoY - 1 + y
                newX = currCoX - 1 + x
                
                if checkCoOrdValid(newX, newY) and coOrdsNotTheSame(currCoX, currCoY, newX, newY):
                    #Check if empty space on the otherside of piece in selected spot
                    if isEmpty(newX, newY, board) == False:
                        #Work out new co-ordinates
                        newX = currCoX + calculateJumpPosition(x)
                        newY = currCoY + calculateJumpPosition(y)
                        
                        #Add to potential moves if valid
                        if checkCoOrdValid(newX, newY) and isEmpty(newX, newY, board) == True:
                            moves.append((newX, newY, True))
    return moves

def checkCoOrdValid(coX, coY):
    """
    Check if the current iteration will go outside the array boundaries
    
    INPUT:
        :param coX: Integer, the x coordinate on the board
        :param coY: Integer, the y coordinate on the board
    
    OUTPUT:
        Boolean, dependent upon whether the input of the user is within range
    """

    if coX < BOARD_MIN_SIZE or coX > BOARD_MAX_SIZE - 1:
        return False
    elif coY < BOARD_MIN_SIZE or coY> BOARD_MAX_SIZE - 1:
        return False
    else:
        return True


def isEmpty(coX, coY, board):
    """
    Check to see if there are any pieces within the specified co-ordinates

    INPUT:
        :param coX: Integer, the x coordinate for the space that is being checked
        :param coY: Integer, the y coordinate for the space that is being checked
        :param board: 2D list, the current board state
    
    OUTPUT:
        Boolean, dependent on whether the space is empty or contains a piece 
    """
    if board[coY][coX] == "-":
        return True
    else:
        return False

def checkIfSameColourPiece(coX, coY, colPiece, board):
    """
    Check to see if position contains one of the player's pieces at the specified co-ordinates

    INPUT:
        :param coX: Integer, the x coordinate for the space that is being checked
        :param coY: Integer, the y coordinate for the space that is being checked
        :param board: 2D list, the current board state
    
    OUTPUT:
        Boolean, dependent on whether the space is empty or contains a piece 
    """
    if board[coY][coX] == colPiece:
        return True
    else:
        return False

def calculateJumpPosition(diff):
    """
    Used to calculate the potential move of a piece performing a hopping action over an adjacent piece

    INPUT:
        :param diff: Integer, a number between 0 and 2
   
    OUTPUT:
        The amount that needs to be added onto the coordinate to find the jumping position
    """
    if diff == 0:
        return -2
    elif diff == 1:
        return 0
    else:
        return 2


def coOrdsNotTheSame(currX, currY, newX, newY):
    """
    Check if the input co-ordinates are equal

    INPUT:
        :param currX: Integer, the x coordinate of where the piece is located on the board
        :param currY: Integer, the y coordinate of where the piece is located on the board
        :param newX: Integer, the x coordinate of the position to be moved to on the board
        :param newY: Integer, the y coordinate of the position to be moved to on the board
    
    OUTPUT:
        Boolean, depending on whether the current coordinates are both equal to the new coordinates
    """
    if currX == newX and currY == newY:
        return False
    else:
        return True

def winCheck(piece, board):
    """
    Check if the current player has won the game

    INPUT:
        :param piece: String, the current player that the check is being performed for, either b or w
        :param board: 2D list, the current board state
    
    OUTPUT:
        Boolean, depending upon whether the player has won or not
        Integer, if the user has inputted an invalid value
    """
    if piece == "b":
        #Check if all black pieces are in the opposite corner
        #Adjust amount of pieces that need to be checked on each row
        amountToCheck = 2
        
        for y in range(3, BOARD_MAX_SIZE): 
            for x in range(1, amountToCheck):
                #Check if current position matches piece
                if board[y][-x] != piece:
                    return False
            
            amountToCheck += 1

        return True
    
    elif piece == "w":
        #Check if all white pieces are in the opposite corner
        #Adjust amount of pieces that need to be checked on each row
        amountToCheck = 3
        
        for y in range(BOARD_MIN_SIZE,3): 
            for x in range(BOARD_MIN_SIZE, amountToCheck):
                
                if board[y][x] != piece:
                    return False
            
            amountToCheck -= 1
        return True
    else:
        #Error case for invalid inputs
        return -1

def getAllPotentialMoves(colPiece, board):
    """
    Get all potential immediate moves from one player's pieces.

    INPUTS:
        :param colPiece: String, the pieces that will used when calculating potential moves
        :param board: 2D list, the current board state
    
    OUPUT:
         List of Tuples, containing a unique identifier with the list of potential moves for each piece
    """
    moves = []
    currPieceNo = 0
    for y in range(0, BOARD_MAX_SIZE):
        for x in range(0, BOARD_MAX_SIZE):
            if board[y][x] == colPiece:
                currPieceNo += 1
                
            #If this is true then all pieces have already been found
            elif currPieceNo == MAX_PIECE_NO:
                y = BOARD_MAX_SIZE
                break
                
    return moves

def performPlayerTurn(colPiece, currBoard, hasJumped, selectedX, selectedY) :
    """
    Where the players turns will be performed to follow D.R.Y.

    INPUT:
        :param colPiece: String, the colour of the player's pieces
        :param currBoard: 2D list, the current board state
        :param hasJumped: Boolean, whether or not a previous turn has taken place in which the move involved jumping over another piece
        :param selectedX: Integer, used if hasJumped = True, the current x position of the piece to be moved
        :param selectedX: Integer, used if hasJumped = True, the current y position of the piece to be moved

    OUTPUT:
        List of tuples, containing the current cooridinates of the piece and the destination coordinates
    """
    printBoard(currBoard)

    #Repeat until user has selected where they want to move a piece
    selectedMove = False
    while selectedMove != True:
        #Ask the user for input only if the piece has not jumped
        if hasJumped == False:
            #Repeat until user input is valid
            validMove = False
            while validMove == False:
                try:
                    #Get the x and y coordinates of the move
                    userInput = input(f"Select a {colPiece!r} piece, using the coordinates in the format x,y (e.g. 0,1): ")
                
                    #Get the values from the string
                    selectedX = int(userInput[0])
                    selectedY = int(userInput[2])

                    #Check if the currently selected position is:
                    #One of the player's pieces
                    if checkIfSameColourPiece(selectedX, selectedY, colPiece, currBoard):
                        #And, can perform a legal move
                        if len(getPotentialMoves(selectedX, selectedY, currBoard, hasJumped)) > 0:
                            validMove = True
                except:
                    print("You can only enter numbers, that are in the range of 0 - 5 and in the format x,y")
        moves = getPotentialMoves(selectedX, selectedY, currBoard, hasJumped)

        #Make where the user can move their piece easier to see
        potentialBoard = createPotentialMovesBoard(moves, currBoard)
        printBoard(potentialBoard)

        #Fix board carrying over potential moves into actual board
        for x,y,_ in moves:
            potentialBoard[y][x] = "-"

        #A error check for where a piece is being moved
        noMoves = False
        validInput = False
        moveInput = None
        while validInput == False:
            try:
                #Ask user to choose one of the potential moves
                if len(moves) == 1 and hasJumped == False:
                    moveInput = int(input("To confirm the move type 1, to change type 0: "))
                
                elif hasJumped == False:
                    moveInput = int(input(f"To select a move, enter a number between 1 and {len(moves)} or 0 to select another piece: "))
                
                elif hasJumped == True and len(moves) >=1:
                    moveInput = int(input(f"To select a move, enter a number between 1 and {len(moves)} or 0 to keep your piece where it is: "))
                #In case the user moves their piece to an area with no potential moves
                elif len(moves) == 0:
                    noMoves = True

                #Make sure input is correct
                if type(moveInput) != int or moveInput < 0 or moveInput > len(moves):
                    validInput = False
                else:
                    validInput = True
            except:
                print("Sorry, but only integers are accepted (e.g. 0,1,2,3...)")
                validInput = False

        #Check whether user has selected a piece or wishes to 
        if moveInput <= 0 and hasJumped == False or moveInput > len(moves) and hasJumped == False:
            selectedMove = False

        #Allow the piece to remain where it is
        elif hasJumped == True and noMoves == True or hasJumped == True and moveInput == 0:
            moves.append((selectedX,selectedY, False))
            moveInput = len(moves)
            selectedMove = True
        else:
            selectedMove = True
        
        
    #Return the coordinates of where the piece currently is and needs to go
    action = [(selectedX,selectedY), moves[moveInput-1]]
    return action
            
def createPotentialMovesBoard(potentialMoves, board):
    """
    Show visually where the user can potentially move their pieces

    INPUT:
        :param potentialMoves: List of tuples, contains the x,y coordinates of potential moves from a selected piece
        :param board: 2D list, the board in its current state
    OUTPUT:
        2D list of strings, potential moves added to the board through the use of numerical symbols to distinguish themselves
    """
    #Visually show where the piece can be moved
    i = 0
    for potX,potY,_ in potentialMoves:
        i += 1
        board[potY][potX] = str(i)
    return board

"""
◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘
◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘
Where the game is
◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘
◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘◘
"""
def demonstration():
    replay = True
    while replay != False:
        print("Hello, and welcome to Halma!")

        validInput = False
        plyrOnePiece = ""
        plyrTwoPiece = ""


        while validInput != True:
            #Have the players choose which colour they want to play as
            selectedPiece = input("Player 1, which colour do you wish to be, black or white (b/w):")
        
            #Assign pieces based on input, and allow to proceed if valid
            if selectedPiece.lower() == "b" or selectedPiece.lower() == "black":
                plyrOnePiece = "b"
                plyrTwoPiece = "w"
                validInput = True

            elif selectedPiece.lower() == "w" or selectedPiece.lower() == "white":
            
                plyrOnePiece = "w"
                plyrTwoPiece = "b"
                validInput = True

        #Set up the board for play
        board = initBoard()

        hasWon = False
        #For when the piece has jumped
        hasJumped = False
    
        destX = None
        destY = None

        #Plays the game until one of the players has reached a winning state
        while hasWon == False:
            hadTurn = False
            #Play until piece hasn't jumped
            while hasJumped == True or hadTurn == False:

                #Play their turn to get the coordinates of where the pieces are to be moved
                playerOneAction = performPlayerTurn(plyrOnePiece, board, hasJumped, destX, destY) 
                currX,currY = playerOneAction[0]
                destX,destY,hasJumped = playerOneAction[1]
                
                board = movePiece(currX,currY,destX,destY,board)
                #Allows atleast one move to happen
                hadTurn = True
                #Check to see if the game still needs to proceed
                if winCheck(plyrOnePiece, board) == True:
                    print("Well done player one!")
                    hasWon = True

            #Continue if player one hasn't won
            if hasWon == False:
                hadTurn = False

                #Play until the move doesn't jump over another piece
                while hasJumped == True or hadTurn == False:
                    #Play their turn to get the coordinates of where the pieces are to be moved
                    playerTwoAction = performPlayerTurn(plyrTwoPiece,board,hasJumped, destX, destY) 
                    currX,currY = playerTwoAction[0]
                    destX,destY,hasJumped = playerTwoAction[1]
                    
                    board = movePiece(currX,currY,destX,destY,board)
                    #Allows atleast one move to happen
                    hadTurn = True
                    
                    if winCheck(plyrTwoPiece, board) == True:
                        print("Well done player two!")
                        hasWon = True

        #Determine whether a game needs to be played
        response = input("Would you like to play again? (y/n): ")

        if response.lower() == "y" or response.lower() == "yes":
            print("Good luck in your next game!")
        else:
            print("Thank you for playing, I hope you enjoyed and play again soon!")
            replay = False
        return True