import copy
import random
import sys

import Board as halma
"""
Created by Joe Benjamin Hopkins Panes, 961006, 2020

*********************
PYTHON VERSION: 3.7.4

BUILT USING ANACONDA VERSION: 3.7, October 2019

*********************
"""
#The maximum amount of moves that can be explored per node
MAX_TURNS = 50000

WINNING_STATE_VALUE = 200000

#Used in the heursitic to desentivise the outer edges of the board
POSITION_WEIGHTING = [[ 1,   1,   1, 0.8, 0.7, 0.6],
                      [ 1,   1,   1,  1,  0.8, 0.7],
                      [ 1 ,  1,   1,  1,   1,  0.8],
                      [0.8,  1,   1,  1,   1,   1],
                      [0.7, 0.8,  1,  1,   1,   1],
                      [0.6, 0.7, 0.8, 1,   1,   1]]

def getPotentialMoves(currCoX, currCoY, board, hasJumped):
    """
    Check whether the selected piece can perform a legal move.
    This is the same as the Board.py, however, it will retrieve all positions that a piece can
    be moved, ignoring the need for human interaction. 

    INPUT:
        :param currCoX: Integer, the x coordinate of the board position
        :param currCoY: Integer, The y coordinate of the board position
        :param board: 2D list, The current board state
        :param hasJumped: Boolean, whether or not the piece has moved across another piece before
    
    Output:
        A list of tuples, coordinates of legal move(s) that can be performed from the current position along whether or not a jump was performed prior
    """
    moves = []
    #Make sure all moves are not repetitions
    previouslyVisitedPositions = {}
    if board[currCoY][currCoX] == "-":
        return moves
    elif hasJumped == False:
        #Check area around piece
        for y in range(0,3):
            for x in range(0,3):
                newX = None
                newY = None

                newY = currCoY - 1 + y
                newX = currCoX - 1 + x

                if halma.checkCoOrdValid(newX, newY) and halma.coOrdsNotTheSame(currCoX, currCoY, newX, newY):
                    #Add to potential moves if valid
                    if halma.isEmpty(newX, newY, board) == True:
                        moves.append((newX, newY, False))
                        #Add to previously explored moves
                        previouslyVisitedPositions[f"{newX}, {newY}"] = "Boop"
                    #Check if empty space on the otherside of piece in selected spot
                    else:
                        #Work out new co-ordinates
                        newX = currCoX + halma.calculateJumpPosition(x)
                        newY = currCoY + halma.calculateJumpPosition(y)

                        if halma.checkCoOrdValid(newX, newY) and halma.isEmpty(newX, newY, board) == True:
                            moves.append((newX, newY, True))

                            #Add to previously explored moves
                            previouslyVisitedPositions[f"{newX}, {newY}"] = "Boop"

                            newBoard = copy.deepcopy(board)
                            #Swap the values at the given destinations
                            newBoard[currCoY][currCoX], newBoard[newY][newX] = newBoard[newY][newX], newBoard[currCoY][currCoX]
                            
                            #Check whether this move can lead to further jumps
                            potentialMoves = getPotentialMoves(newX, newY, newBoard, True)

                            #Only add moves that haven't been previously been explored
                            for currMove in potentialMoves:
                                moveX = currMove[0]
                                moveY = currMove[1]
                                
                                if not previouslyVisitedPositions.get(f"{moveX}, {moveY}"):
                                    moves.append(currMove)

    #If they have moved, only allow them to jump over other pieces to move
    else:
        #Check around piece if they have move before
         for y in range(0,3):
            for x in range(0,3):
                newX = None
                newY = None

                newY = currCoY - 1 + y
                newX = currCoX - 1 + x
                
                if halma.checkCoOrdValid(newX, newY) and halma.coOrdsNotTheSame(currCoX, currCoY, newX, newY):
                    #Check if empty space on the otherside of piece in selected spot
                    if halma.isEmpty(newX, newY, board) == False:
                        #Work out new co-ordinates
                        newX = currCoX + halma.calculateJumpPosition(x)
                        newY = currCoY + halma.calculateJumpPosition(y)
                        
                        #Add to potential moves if valid
                        if halma.checkCoOrdValid(newX, newY) and halma.isEmpty(newX, newY, board) == True:
                            moves.append((newX, newY, True))
    return moves

def getAllPotentialMoves(colPiece, board):
    """
    Get all potential immediate moves from one player's pieces.
    Same as Board.py, but uses this version of getPotentialMoves

    INPUTS:
        :param colPiece: String, the pieces that will used when calculating potential moves
        :param board: 2D list, the current board state
    
    OUPUT:
         List of Tuples, containing a unique identifier with the list of potential moves for each piece
    """
    moves = []
    currPieceNo = 0
    for y in range(0, halma.BOARD_MAX_SIZE):
        for x in range(0, halma.BOARD_MAX_SIZE):
            if board[y][x] == colPiece:
                currPieceNo += 1
                pieceMoves = getPotentialMoves(x, y, board, False)

                #Only add moves that have a potential
                if len(pieceMoves) > 0:
                    moves.append((currPieceNo, (x, y), pieceMoves))
            #If this is true then all pieces have already been found
            elif currPieceNo >= halma.MAX_PIECE_NO:
                y = halma.BOARD_MAX_SIZE
                break
                
    return moves

def makeARandomMove(board, colPiece):
    """
    Generate and make a move from the current board state

    INPUTS:
        :param board: 2D List, the current board state
        :param colPiece: String, the piece of the current player
    OUTPUT:
        A tuple, contains co-ordinates of where a piece is and where it needs to be
    """
    #Randomly select a move to play
    foundMove = False
    while foundMove == False:
        piecePos = []
        #Iterate over board to find pieces
        for y in range(halma.BOARD_MAX_SIZE):
            for x in range(halma.BOARD_MAX_SIZE):
                if board[y][x] == colPiece:
                    piecePos.append((x,y))

        random.shuffle(piecePos)
        currX, currY = piecePos[0]
        currPositionMoves = getPotentialMoves(currX, currY, board, False)
        
        if len(currPositionMoves) > 0:
            destX, destY, _ = random.choice(currPositionMoves)

            #In the case where the move ends in the same position, reduce its frequency of play
            if currX == destX and currY == destY:
                chance = random.randint(0, 100)
                if chance >=85:
                    foundMove = True
            else:
                foundMove = True
    
    #Update piece positions
    if foundMove == True:    
        board = halma.movePiece(currX, currY, destX, destY, board)
    return board

def scoreBoardState(prevBoard, currBoard, maximisingPlayer, agentPiece, oppPiece, hasJumped):
    """
    Provides an evaluation of the current board state in order to determine its value in relation
    to how advantageous to the current player it is.

    INPUTS:
        :param currBoard: - 2D list, the current board state
        :param maximisingPlayer - Boolean, where we are in the minmax algorithm
        :param currPiece: String, the colour piece of the current player that is being simulated
        :param oppPiece: String, the colour piece of the opponent player
        :param hasJumped: Boolean, Whether the move leading to this board state required crossing over one or more piece(s)
    OUTPUT:
        Integer, a value pertaining how good it is for the current player (higher the better).
    """
    #If a win state, provide the highest or lowest depending upon who its for
    if halma.winCheck(agentPiece, currBoard):
        return WINNING_STATE_VALUE
    elif halma.winCheck(oppPiece, currBoard):
        return -WINNING_STATE_VALUE

    boardValue = 0
    #Score it based upon how close the piece is to available winning positions
    #Iterate over the board
    for y in range(halma.BOARD_MAX_SIZE):
        for x in range(halma.BOARD_MAX_SIZE):
            currPosValue = 0
            
            #Score the current position differently based upon which piece is present
            if currBoard[y][x] != "-":
                if currBoard[y][x] == "w":
                    """
                    Handles white Pieces
                    """
                    #Calculate how far away from a winning position it is
                    if str(x) + "," + str(y) in halma.WHITE_WINNING_POSITIONS:
                        currPosValue += 20
                    else:
                        #Calculate the distance from the current piece to each empty winning position
                        currPosValue += (5 - x) + (5 - y)
                        
                elif currBoard[y][x] == "b":
                    """
                    Handles black pieces
                    """   
                    if str(x) + "," + str(y) in halma.BLACK_WINNING_POSITIONS:
                        currPosValue += 20
                    else:
                        currPosValue += x + y     
                
                #Encourage moves through the middle
                currPosValue = currPosValue * POSITION_WEIGHTING[y][x]
                
                #Check whether current piece is not surrounded by any other piece
                piecesNextToIt = 0
                for adjustY in range(0,3):
                    for adjustX in range(0,3):
                        newX = None
                        newY = None

                        newY = y - 1 + adjustY
                        newX = x - 1 + adjustX

                        if halma.checkCoOrdValid(newX, newY) and halma.coOrdsNotTheSame(x, y, newX, newY):

                            if halma.isEmpty(newX, newY, currBoard) == False:
                                piecesNextToIt += 1
                                adjustX = 3
                                adjustY = 3
                
                #If a lone piece, then decentivize this (since it take more moves to catch up)
                if piecesNextToIt == 0:
                    currPosValue = currPosValue * 0.25
                
                boardValue += currPosValue
                

    #Incentivse jumping over pieces
    if hasJumped:
        boardValue = boardValue * 1.05
    
    #Check whether the resulting position is the same state
    if str(prevBoard) == str(currBoard):
        boardValue = boardValue * 0.95

    return boardValue

def addPositionScore(agentPiece, oppPiece, currPosPiece, positionValue):
    """
    Depending on the where in the minmax algorithm we are, and who's piece is in the given position

    INPUTS:
        :param agentPiece: String, the colour pieces that the agent is playing with
        :param oppPiece: String, the colour pieces that the opponent is playing with
        :param currPosPiece: String, the colour of the piece at the current position in the board
        :param positionValue: Integer, the value of the current piece in the board

    OUTPUT:
        Integer, which will either be positive or negative, based upon maximisingPlayer and which piece is at position    
    """

    if agentPiece == currPosPiece and agentPiece == "b":
        return positionValue
    elif oppPiece == currPosPiece:
        return -positionValue

def minMax(board, depth, alpha, beta, maximisingPlayer, agentPiece, oppPiece, hasJumped):
    """
    Perform the MinMax search algorithm recursively
    Using:
        1. Alpha-beta Pruning - Allows the tree to not need to look at all potential board states, where if a path suddenly provides a less valuable 
                                position, it can stop where it is and provide its current best value.
        2. Killer Heuristic - Where in tangent with Alpha-Beta Pruning, the boards that provide the best current value are provided to the left most of the list
    INPUTS:
        :param board: 2D list, the current potential board state
        :param depth: Integer, how far ahead it looks ahead in potential board states
        :param alpha: Integer, finds the largest value quickly without needing to search all board states
        :param beta: Integer, finds the smallest value quickly without needing to search all board states
        :param maximisingPlayer: Boolean, whether we are looking for the minimum or maximum of the current round of potential board states
        :param agentPiece: String, the colour piece used by the computer
        :param oppPiece: String, the colour piece used by the computer's opponent
        :param hasJumped: Boolean, whether the move that lead to the current board state involved a jump

    OUTPUT:
        returns an integer, which provides a high or low value (depending upon maximisingPlayer) to guide the decision to select a given move
    """
    #Determine the score of the current state when it reaches the
    #Terminal States or reaches max depth
    if depth <= 0 or halma.winCheck(agentPiece, board) or halma.winCheck(oppPiece, board):
        score = 0
        #Check if winning state, add value to score
        if halma.winCheck(agentPiece, board):
            score += WINNING_STATE_VALUE
        elif halma.winCheck(oppPiece, board):
            score -= WINNING_STATE_VALUE

        if depth <= 0:
            score += scoreBoardState(None, board, maximisingPlayer, agentPiece, oppPiece, hasJumped)

        return score

    
    if maximisingPlayer:
        #Where the minmax algorithm is performed
        #Get list of moves
        childStates = findLegalMoves(board, maximisingPlayer, agentPiece, oppPiece)

        #Max agent
        #Play out states until terminal (or depth = 0), then look for largest
        for board, hasJumped, _ in childStates:
            eval = minMax(board, depth - 1, alpha, beta, False, agentPiece, oppPiece, hasJumped)
            if alpha >= beta:
                break #prune
            alpha = max(alpha, eval)
        return alpha
    else:
        #Where the minmax algorithm is performed
        #Get list of moves
        childStates = findLegalMoves(board, maximisingPlayer, oppPiece, agentPiece)
        #Min agent
        #Play out states until terminal (or depth = 0), then look for smallest
        for board, hasJumped, _ in childStates:
            eval = minMax(board, depth - 1, alpha, beta, True, agentPiece, oppPiece, hasJumped)
            if alpha >= beta:
                break #prune
            beta = min(beta, eval)
        return beta

def findLegalMoves(board, maximisingPlayer, agentPiece, oppPiece):
    """
    Get all potential moves performable by the current player, that lead to unique board states

    INPUTS: 
        :param board: 2D list, the current board state
        :param maximisingPlayer: Whether the current turn is the agent or opponent
        :param agentPiece: String, the colour of the agent's pieces
        :param oppPiece: String, the colour of the opponet's pieces
    OUTPUT:
        2D list, containing a tuples showing the each potential board state and a boolean as to whether a jump was necessary to reach it
    """
    uniqueMoves = {}
    
    potentialMoves = []
    if maximisingPlayer:
        moves = getAllPotentialMoves(oppPiece, board)
    else:
        moves = getAllPotentialMoves(agentPiece, board)
   
    #Iterate through the potential moves of each piece
    for currPiece in range(0, len(moves)):
        for currMove in range(1, len(moves[currPiece][2])):
            currBoard = copy.deepcopy(board) 
            #Extract move information from the list
            currCoX, currCoY = moves[currPiece][1]
            newCoX, newCoY, hasJumped = moves[currPiece][2][currMove]
            
            newBoard = halma.movePiece(currCoX, currCoY, newCoX, newCoY, currBoard)

            score = scoreBoardState(board, newBoard, maximisingPlayer, agentPiece, oppPiece, hasJumped)
            
            #Reduce the looked at moves to only those that lead to unique positions that haven't been seen before
            if not uniqueMoves.get(str(newBoard)):
                potentialMoves.append((newBoard, hasJumped, score))
                uniqueMoves[str(newBoard)] = "Yep"

    #Sort it so that the boards that provides the best values as the first seen in the list
    potentialMoves.sort(key=getBoardValue)

    if maximisingPlayer == False:
        #When we're looking at the minimising player, we are concered more with the smallest values over the largest
        potentialMoves = potentialMoves[::-1]

    return potentialMoves


def getBoardValue(boardStateInfo):
    """
    Used for the sorting of the potential moves in findLegalMoves method
    
    INPUTS:
        :param boardStateInfo: 2D list of tuples, containing board state, whether a jump was used to get to current board, the value of the current board
    OUTPUT:
        Returns the value of the current board state
    """
    return boardStateInfo[2]


def performMinMax(board, depth, agentPlayer, oppPlayer):
    """
    The main point of external interaction with the Minmax algorithm, performs it with all of the intended extra features to the
    standard algorithm, such as:
    1. Scoring intermediate states
    2. Alpha-Beta Pruning
    3. Killer Moves
    4. Iterative deepening

    INPUTS:
        :param board: 2D list, the current board state
        :param depth: Integer, the total depth to be explored by the MinMax algorithm
        :param agentPlayer: String, the colour of the agents pieces
        :param oppPlayer: String, the colour of the opponents pieces
    
    OUTPUT:
        returns a list containing the value of the selected board state and the 2D list of the new board
    """
    potentialMoves = findLegalMoves(board, False, agentPlayer, oppPlayer)
    bestValue = [-sys.maxsize, board]
    
    #Iterative deepening
    #for currMaxDepth in range(depth): 
    for currBoard, hasJumped, _ in potentialMoves:
        value = minMax(board, depth, -sys.maxsize, sys.maxsize, False, agentPlayer, oppPlayer, hasJumped)

        if value > bestValue[0]:
            bestValue = [value, currBoard]

    return bestValue

def demonstration():
    """
    Was used for testing, but showcases how this works on its own
    """
    """board = [["b","b","b","-","-","-"],
            ["b","b","-","-","-","-"],
            ["b","-","-","-","-","w"],
            ["-","-","-","-","-","-"],
            ["-","-","-","-","-","-"],
            ["-","-","-","-","-","-"]]"""

    board = halma.initBoard()
    counter = 0
    try:
        while True:
            print(f"<----{counter}---->")
            """halma.printBoard(board)
            hadTurn = False
            hasJumped = False
            destX = None
            destY = None

            #Play until piece hasn't jumped
            while hasJumped == True or hadTurn == False:

                #Play their turn to get the coordinates of where the pieces are to be moved
                playerOneAction = halma.performPlayerTurn("w", board, hasJumped, destX, destY) 
                currX,currY = playerOneAction[0]
                destX,destY,hasJumped = playerOneAction[1]
                
                board = halma.movePiece(currX,currY,destX,destY,board)
                #Allows atleast one move to happen
                hadTurn = True
                #Check to see if the game still needs to proceed
                if halma.winCheck("w", board) == True:
                    print("Well done player one!")
                    hasWon = True"""

            board = makeARandomMove(board,"w")
            """bestVal = performMinMax(board, 3, "w", "b")
            board = bestVal[1]
            print("w " + str(bestVal[0]))"""
            bestVal = performMinMax(board, 3, "b", "w")
            board = bestVal[1]


            print("b " + str(bestVal[0]))
            print("||||||||||||||||||||||||||||||||||||")
            
            counter += 1
            if halma.winCheck("b", board) or halma.winCheck("w", board):
                break
        halma.printBoard(board)
        print(counter)
    except:       
        halma.printBoard(board)
        print(counter)
    
#demonstration()