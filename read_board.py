import cv2
import imageio
import glob
import os
import datetime
import numpy as np

from PIL import Image
"""
Created by Joe Benjamin Hopkins Panes, 2020

*********************
PYTHON VERSION: 3.7.4

BUILT USING ANACONDA VERSION: 3.7, October 2019

MODULE VERSIONS: 
 ______________________________________
|  Module                 |  Version   |
|--------------------------------------|
|  opencv-contrib-python  |  4.2.0.32  |            
|  Pillow                 |  6.2.0     |
|  imageio                |  2.5.0     |
|  numpy                  |  1.16.2    |
| ________________________|____________|       
*********************
"""

"""
These are constants used for a 6x6 playable board, on a 8x8 chessboard adjust these numbers as needed
for varying sizes of board
"""
SIZE_OF_BOARD = 8
#Used for findChessboardCorners()
NO_OF_INNER_CORNER = 7
NO_OF_SQUARES_PER_ROW = 6
NO_OF_ROWS = 6

def initialise(webcam):
    """
    Get the captured image ready inorder to extract the structure of the board

    INPUTS-
        :param webcam: a video object that allows the capturing of images using a webcam
    OUTPUTS-
        A true or false depending on whether the board was found, and if true a list 
    """
    input("Press enter when the pieces are cleared from the board")
    #Capture image
    check, frame = webcam.read()

    #Correct image to be more inline with real life appearance
    correctedImage = cv2.cvtColor(frame, cv2.CALIB_CB_NORMALIZE_IMAGE)

    found, corners = calibrateBoard(correctedImage)
    
    return found, corners
    

def calibrateBoard(image):
    """
    Find the points for each corner within the board.
    Has to be done when there are no pieces on the board.

    INPUTS -
        :param image: An 8 bit image of a checkerboard

    OUTPUTS -
        List of all the points within the image and the boolean for whether or not the
        board was recognised
    """
    #Search the board with an exchaustive approach, where the image is normalised and adaptive thresholding
    #Used to account for any difference in enviromental lighting
    found, corners = cv2.findChessboardCorners(image,(NO_OF_INNER_CORNER, NO_OF_INNER_CORNER),
                        flags = cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE 
                        + cv2.CALIB_CB_EXHAUSTIVE)
    return found, corners

def printBoard(image, found, corners):
    """
    Present the findings of the findCheckerboardCorners function
    as a visual representation

    INPUTS -\n
        :param image: Any image (RGB, Grey, etc.) of the board used 
        :param found: boolean of whether a board was found in an image
        :param corners: list of co-ordinates to points within an image found to be corners
    
    OUTPUT -
        An image that has the corners of the list placed onto the image
    """
    return cv2.drawChessboardCorners(image,(NO_OF_INNER_CORNER,NO_OF_INNER_CORNER),corners,found)

def checkIfPointValid(point):
    """
    Checks the given point value to make sure it isn't invalid or last point in a row

    Input:
        :param point: An integer between 0 and 48
    
    Output:
        A boolean depending upon whether it is a valid input
    """
    #Check if the input is outside of the list range
    if point < 0:
        return False
    elif point > (NO_OF_INNER_CORNER * NO_OF_INNER_CORNER-1):
        return False

    #Check that point is an int
    if type(point) != int:
        return False

    #Check if the input is equal to a point at the end of the row
    if point == NO_OF_INNER_CORNER - 1:
        return False
    else:    
        for row in range(SIZE_OF_BOARD):
            if point == NO_OF_INNER_CORNER * row - 1:
                return False
    
    #If it reaches here, the point is valid
    return True




def getSquareFromImage(image, corners, point, prefix, square):
    """
    Crops down the image to a given square, which is worked out by using the point given
    then indexes into the corners list and extracts the necessary co-ordinates from this.\n
    
    

    Input:\n
        :param image:  A Pillow Image object\n
        :param corners: A list of corners given by the function findChessboardCorners\n
        :param point: An integer that is used to retrive the top left corner of the square (e.g. point 0 would provide square 0, p9 would provide s9)\n
        :param prefix: A string that is used when naming the image file\n
        :param squar: An integer that is used for the naming of the image (see the s values above)\n

        For example, if filename = "cropped" and square = 1, then the file will be saved as "cropped1.jpg"\n

        For a board of 6x6, the points will look like:
            s[No.] = The square number, and what should be the input for the name of that cropped square
            p[No.] = The point number, and what should be the input 
                                        
           p0----p1----p2----p3----p4----p5----p6  \n
            | s1  | s2  | s3  | s4  | s5  | s6  |  \n
           p7----p8----p9---p10---p11---p12---p13  \n
            | s7  | s8  | s9  | s10 | s11 | s12 |  \n
           p14---p15---p16---p17---p18---p19---p20 \n
            | s13 | s14 | s15 | s16 | s17 | s18 |  \n
           p21---p22---p23---p24---p25---p26---p27 \n
            | s19 | s20 | s21 | s22 | s23 | s24 |  \n
           p28---p29---p30---p31---p32---p33---p34 \n
            | s25 | s26 | s27 | s28 | s29 | s30 |  \n
           p35---p36---p37---p38---p39---p40---p41 \n
            | s31 | s32 | s33 | s34 | s35 | s36 |  \n
           p42---p43---p44---p45---p46---p47---p48 \n

    Output:
        An image that has fours points of the square marked on the image\n
        Boolean on whether or not it was able to successfully print a valid square\n
    """
    if checkIfPointValid(point):
        #Get the list indexes for the corners of the squares, top left and bottom right
        p1 = point
        p2 = point + NO_OF_INNER_CORNER + 1

        #Get all the needed dimensions from the corner list
        p1Corner = corners[p1][0]
        p2Corner = corners[p2][0]

        #Convert corners from floats to ints
        pointXYs = int(p1Corner[0]), int(p1Corner[1]), int(p2Corner[0]), int(p2Corner[1])
        imCrop = image.crop(pointXYs)

        imCrop.save(prefix + str(square) + ".jpg",'JPEG', quality= 100)
    else:
        return False
    
    return True

def getEachSquareFromImage(image, corners, prefix):
    """
    Iterate over the entire board to crop each relevant square

    Input:
        :param image: Any image that is a picture of the board used to obtain the corners
        :param corners: List of points of each inner corner of the board
        :param prefix: A string which each of the output images will be named

    Output:
        A large amount of images saved with the prefix
        A boolean of whether everything worked as intended
    """
    #Used in tandem with the prefix for naming
    currSquare = 1

    #Work through all relevant points on the board (currently setup for 6x6 board)
    for point in range(0, 41):
        validPoint = getSquareFromImage(image, corners, point, prefix, currSquare)
        if validPoint:
            currSquare += 1

    #If everything goes well
    return True

def isPieceInSquare(prefix, currBoardPos):
    """
    Discovers whether there is a piece in the current square on the board
    It does this through a mixture of:
                    1. Adaptive Thresholding
                    2. Canny edge detection (see HoughCircles documentation)
                    3. Hough Circle Transform
    
    In order to function properly this function requires the existance of a grey image and colour image of the current board position

    INPUT:
        :param prefix: String, the name shared by the set of images
        :param currBoardPos: An integer between 1 and NO_SQUARE_PER_ROW * NO_OF_ROWS
    
    OUTPUT:
        If a piece is found, it returns True boolean and the x and y co-ords of the centre of the circle
        Otherwise, it returns a False boolean and two 0s

    """
    boardPos = imageio.imread(prefix + str(currBoardPos) +".jpg")
    
    #Prepare image by converting it to grey scale
    boardPos = cv2.cvtColor(boardPos, cv2.COLOR_BGR2GRAY)
    boardPos = cv2.adaptiveThreshold(boardPos,230, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,3)
    #Prepare image by converting it to grey scale
    

    #boardPos = cv2.adaptiveThreshold(boardPos,230, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,3)
    
    #Perform a Houghman Transform Circle on the image, (with a Canny Edge Detection)
    circle = cv2.HoughCircles(boardPos, cv2.HOUGH_GRADIENT, 1, 20, param1=220, param2=12, minRadius = 11,maxRadius=15)

    if circle is not None:
        #Convert np.array() to list, so that it can be broken down
        cirList = circle.tolist()
        x = int(round(cirList[0][0][0]))
        y = int(round(cirList[0][0][1]))
        r = int(round(cirList[0][0][2]))
        
        #Draw what was detected over the colour image, along with the centre of the circle
        originalImage = imageio.imread(prefix+str(currBoardPos)+".jpg")
        cv2.circle(originalImage, (x, y), r, (0, 255, 0), 2)
        cv2.rectangle(originalImage, (x - 2, y - 2), (x + 2, y + 2), (0, 255, 0), -1)


        imageio.imwrite("circleDetection"+ str(currBoardPos) +".jpg", originalImage, quality= 100)
        
        return True, x, y
    return False, 0, 0

def convertSquareToList(currSquare):
    """
    Convert the number of a square (see diagram in getSquareFromImage) to that usable by the list used to store
    the board state list.

    INPUT:
        :param currSquare: An integer between 1 and NO_SQUARES_PER_ROW and NO_OF_ROWS
    
    OUTPUT:
        returns the X and Y of the square's position within the list 
    """

    #Work out where in the list the square is located
    if currSquare < 1 or currSquare > (NO_OF_ROWS * NO_OF_SQUARES_PER_ROW):
        return None

    elif currSquare < NO_OF_SQUARES_PER_ROW + 1 and currSquare > 0:
      return  int(currSquare-1), 0
    
    elif currSquare < NO_OF_SQUARES_PER_ROW *2 +1:
        return int(currSquare - NO_OF_SQUARES_PER_ROW)-1, 1
    
    elif currSquare < NO_OF_SQUARES_PER_ROW *3 +1:
        return int(currSquare - NO_OF_SQUARES_PER_ROW * 2)-1, 2
    
    elif currSquare < NO_OF_SQUARES_PER_ROW *4 +1:
        return int(currSquare - NO_OF_SQUARES_PER_ROW * 3)-1, 3
    
    elif currSquare < NO_OF_SQUARES_PER_ROW *5 +1:
        return int(currSquare - NO_OF_SQUARES_PER_ROW * 4)-1, 4
    
    elif currSquare < NO_OF_SQUARES_PER_ROW *6 +1:
        return int(currSquare - NO_OF_SQUARES_PER_ROW * 5)-1, 5

def convertBoardToList(prefix):
    """
    Working through each of the squares of the board that have had a circle detected in them
    and find out which colour piece is in that square. Then, add that piece to the list.

    INPUT:
        :param prefix: String, the standard name amongst the set of images files of the corresponding board state

    OUTPUT:
        returns a 6x6 list of the current board state containing the correctly labelled piece positions 
    """
    board = []
    for y in range(0, NO_OF_ROWS):
        board.append([])
        board[y] = ["-"]*NO_OF_SQUARES_PER_ROW

    for currBoardPos in range(1,37):
        worked, currPieceX, currPieceY = isPieceInSquare(prefix, currBoardPos)

        if worked:
            #Get the curr Square
            currSquare = imageio.imread(prefix+str(currBoardPos)+".jpg")

            #Get RGB values from centre of detected circle
            B,G,R = currSquare[currPieceY][currPieceX]

            average = int(R)+int(G)+int(B) / 3
            
            #Work out the colour of the current piece
            #Also,one step of ignoring a False Positive since Black and White pieces will only be of a certain colour range
            x,y = convertSquareToList(currBoardPos)

            if average < 200:
                board[y][x] = "b"
            elif average > 325:
                board[y][x] = "w"
    
    return board
              



    
    
"""
*************************************
*************************************
Test Example
*************************************
*************************************
"""
def demonstration():
    #Initialise video object to use webcam
    webcam = cv2.VideoCapture(0)

    input("Press enter when the pieces are cleared from the board")
    #Capture image
    check, frame = webcam.read()

    found, corners = calibrateBoard(correctedImage)

    imageio.imwrite("testCalib.png", printBoard(correctedImage,found,corners))

    input("Please press enter when you have filled the board, and are ready to proceed")

    #Capture image
    check2, frame2 = webcam.read()

    imageio.imwrite("currBoardPure.jpg", frame2, quality=100)
    pureImage = Image.open("currBoardPure.jpg")

    worked = getEachSquareFromImage(pureImage, corners,"square")
    
    #Change the colour of the image to grey
    correctedImage2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)

    #Save current board state
    imageio.imwrite("currBoardwithPieces.jpg", correctedImage2,quality=100)
    
    #Open using the PIL libraries image to allow for the use of crop() function
    im2 = Image.open("currBoardWithPieces.jpg")
    
    worked = getEachSquareFromImage(im2, corners,"filledBoardSquare")

    board = convertBoardToList("square")

    for y in range(0, NO_OF_ROWS):
        print(board[y])
            
    webcam.release()
    return True

#demonstration()